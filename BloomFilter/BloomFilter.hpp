//
//  BloomFilter.hpp
//  BloomFilter
//
//  Created by Юлия on 20.11.15.
//  Copyright © 2015 Юлия. All rights reserved.
//

template <typename T>
BloomFilter<T>::BloomFilter(size_t size, size_t hashes):filter(size),hashes(hashes) {}
template <typename  T>
BloomFilter<T>::~BloomFilter(){}
template <typename T>
void BloomFilter<T>::add_element(const T& elem) {
    hash<T> hash;
    size_t h1 = hash(elem);
    size_t h2 = 293848582;
    size_t current_h = h1;
    for (size_t i = 0; i < hashes; ++i)
    {
        filter[current_h] = true;
        current_h = (current_h + h2) % filter.size ();
    }
}
template <typename T>
bool BloomFilter<T>::is_presented(const T& elem) const
{
    bool result = true;
    hash<T> hash;
    size_t h1 = hash(elem);
    size_t h2 = 293848582;
    size_t current_h = h1;
    for (size_t i = 0; i < hashes; ++i)
    {
        result = result && filter[current_h];
        current_h = (current_h + h2) % filter.size();
    }
    
    return result;
}
template <typename T>
void BloomFilter<T>::delete_element(const T &elem) {
    if (!is_presented(elem)) {
        return;
    }
    hash<T> hash;
    size_t h1 = hash(elem);
    size_t h2 = 293848582;
    size_t current_h = h1;
    for (size_t i = 0; i < hashes; ++i)
    {
        filter[current_h] = false;
        current_h = (current_h + h2) % filter.size ();
    }
}

