//
//  main.cpp
//  BloomFilter
//
//  Created by Юлия on 20.11.15.
//  Copyright © 2015 Юлия. All rights reserved.
//

#include <iostream>
#include <math.h>
#include "BloomFilter.h"
int main(int argc, const char * argv[]) {
    double p;
    double n;
    cout << "Enter the propability and number of elements" << endl;
    cin>>p>>n;
    BloomFilter<int> filter(-n*log(p)/(log(2)*log(2)),(-n*log(p)/(log(2)*log(2)))/n*log(2));
    filter.add_element(5);
    filter.add_element(2);
    filter.add_element(1);
    filter.add_element(10);
    filter.add_element(25);
    bool result = filter.is_presented(1);
    cout<< result << endl;
    filter.delete_element(1);
    result = filter.is_presented(1);
    cout<< result << endl;
    filter.delete_element(2);
    result = filter.is_presented(2);
    cout<< result << endl;
    return 0;
}
