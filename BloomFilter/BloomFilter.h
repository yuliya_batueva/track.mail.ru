//
//  BloomFilter.h
//  BloomFilter
//
//  Created by Юлия on 20.11.15.
//  Copyright © 2015 Юлия. All rights reserved.
//

#include <iostream>
#include <vector>
#include <functional>
using namespace std;
template <typename T>
class BloomFilter {
private:
    vector<bool> filter;
    size_t hashes;
    BloomFilter(const T& el) = delete;
    BloomFilter& operator=(const BloomFilter& elem) = delete;

public:
    BloomFilter(size_t size, size_t hashes);
    ~BloomFilter();
    void add_element(const T& elem);
    void delete_element(const T& elem);
    bool is_presented(const T& elem) const;
};
#include "BloomFilter.hpp"