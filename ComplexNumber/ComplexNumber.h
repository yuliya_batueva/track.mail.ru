//
//  ComplexNumber.h
//  ComplexNumber
//
//  Created by Юлия on 10.10.15.
//  Copyright © 2015 Юлия. All rights reserved.
//

#include <iostream>
using namespace std;
class ComplexNumber {
private:
    double real;
    double imaginary;
public:
    ComplexNumber();
    ComplexNumber(double realPart, double imaginaryPart);
    ~ComplexNumber();
    ComplexNumber& operator+(const ComplexNumber& num);
    ComplexNumber& operator-(const ComplexNumber& num);
    ComplexNumber& operator*(const ComplexNumber& num);
    ComplexNumber& operator*(const double num);
    ComplexNumber& operator/(const ComplexNumber& num);
    ComplexNumber& operator/(const double num);
    friend ostream& operator<< (ostream& ostr,const ComplexNumber& obj);
    friend istream& operator>> (istream& istr,ComplexNumber& obj);
};