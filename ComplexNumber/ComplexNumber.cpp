//
//  ComplexNumber.cpp
//  ComplexNumber
//
//  Created by Юлия on 10.10.15.
//  Copyright © 2015 Юлия. All rights reserved.
//

#include "ComplexNumber.h"

#include <string>

ComplexNumber::ComplexNumber(): real(0), imaginary(0) {}

ComplexNumber::ComplexNumber(double a, double b):real(a),imaginary(b) {}

ComplexNumber::~ComplexNumber() {}

ostream& operator<< (ostream& ostr,const ComplexNumber& obj) {
    if (obj.imaginary > 0) {
        ostr<<obj.real<<"+"<<obj.imaginary<<"i";
    }
    if (obj.imaginary == 0) {
        ostr<<obj.real;
    }
    if (obj.imaginary < 0) {
        ostr<<obj.real<<obj.imaginary<<"i";
    }
    return (ostr);
}

istream &operator>> (istream &istr, ComplexNumber& obj) {
    string str;
    istr>>str;
    size_t idx;
    double tmp = stod(str, &idx);
    if (idx < str.size()) {
        if (str[idx] == 'i') {
            obj.imaginary = tmp;
            obj.real = 0;
        } else {
            obj.real = tmp;
            obj.imaginary = stod(str.substr(idx));
        }
    } else {
        obj.real = tmp;
        obj.imaginary = 0;
    }
    return istr;
    
}

ComplexNumber& ComplexNumber::operator+(const ComplexNumber& obj){
    real+=obj.real;
    imaginary+=obj.imaginary;
    return *this;
}

ComplexNumber& ComplexNumber::operator-(const ComplexNumber &num) {
    real-=num.real;
    imaginary-=num.imaginary;
    return *this;
}

ComplexNumber& ComplexNumber::operator*(const double num) {
    real*=num;
    imaginary*=num;
    return *this;
}

ComplexNumber& ComplexNumber::operator*(const ComplexNumber &num) {
    real = real*num.real - imaginary*num.imaginary;
    imaginary = imaginary*num.real + real*num.imaginary;
    return *this;
}

ComplexNumber& ComplexNumber::operator/(const ComplexNumber &num) {
    real = (real*num.real + imaginary*num.imaginary)/( num.real*num.real + num.imaginary*num.imaginary);
    imaginary = (imaginary*num.real - real*num.imaginary)/(num.real*num.real + num.imaginary*num.imaginary);
    return *this;
}

ComplexNumber& ComplexNumber::operator/(const double num) {
    real = real/num;
    imaginary = imaginary/num;
    return *this;
}




